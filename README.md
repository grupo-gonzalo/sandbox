# Bienvenido al arenero (sandbox) de las Practicas Profesionalizantes

Este repositorio será utilizado para realizar practicas y talleres con los estudiantes de la materia.

# Modo de trabajo

Los estudiantes no podrán escribir en este repoistorio. Su interacción, será
sólo mediante el uso de bifurcaciones (**fork**), entonces para modificar el repositorio se debe realizar la bifucacion, en un repositorio individual o grupal y luego enviaran la peticion de cambio **merge requests**

### Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Merge Request


## Actividades

1. [directorio `estudiantes/`](./estudiantes)
1. [directorio `equipos/`](./equipos)
1. [directorio `cancionero/`](./cancionero)

